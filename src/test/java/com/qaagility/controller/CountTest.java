package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class CountTest {

	@Test
	public void devideIntTest() throws Exception {
        	//ModelMap map = mock(ModelMap.class);
		int result = new Count().devideInt(10,2);
		assertEquals( "10 / 2 test :" ,5, result);
	}

	@Test
	public void devideIntTest0() throws Exception {
                //ModelMap map = mock(ModelMap.class);
                int result = new Count().devideInt(10,0);
                assertEquals( "10 / 2 test :" ,Integer.MAX_VALUE, result);
        }
                	
}
